import { Controller, Body, Patch, Param, Delete, UsePipes, ValidationPipe, HttpException, HttpStatus } from '@nestjs/common'
import { EmailService } from './email.service'
import { UpdateEmailDTO } from './dto/update-email.dto'

@Controller('email')
export class EmailController {
  constructor(private readonly emailService: EmailService) {}

  @UsePipes(new ValidationPipe())
  @Patch(':id')
  async updateEmail (@Param('id') id: string, @Body() dto: UpdateEmailDTO) {
    try {
      return await this.emailService.updateEmail(id, dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Delete(':id')
  async deleteEmail (@Param('id') id: string) {
    return this.emailService.deleteEmail(id)
  }
}