import { Type } from 'class-transformer'
import { IsArray, IsNotEmpty, IsString, ValidateNested } from 'class-validator'

export class LanguageInfoDTO {
  @IsString()
  @IsNotEmpty()
  name: string

  @IsString()
  @IsNotEmpty()
  level: string
}

export class CreateLanguageDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => LanguageInfoDTO)
  languages: LanguageInfoDTO[]

  @IsString()
  @IsNotEmpty()
  userId: string
}