import { Injectable } from '@nestjs/common'
import { UpdatePhoneDTO } from './dto/update-phone.dto'
import { PrismaService } from 'src/prisma.service'
// import { CreatePhoneDTO } from './dto/create-phone.dto'

@Injectable()
export class PhoneService {
  constructor (private prisma: PrismaService) {}

  // async createPhone (dto: CreatePhoneDTO) {
  //   return this.prisma.phone.create({
  //     data: dto
  //   })
  // }

  async updatePhone (id: string, dto: UpdatePhoneDTO) {
    return this.prisma.phone.update({
      where: { id },
      data: dto
    })
  }

  async deletePhone (id: string) {
    return this.prisma.phone.delete({ where: { id } })
  }
}