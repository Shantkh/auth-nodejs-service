import { IsNotEmpty, IsString } from "class-validator"

export class MessageDTO {
  @IsNotEmpty()
  @IsString()
  content: string

  @IsNotEmpty()
  @IsString()
  receiverId: string

  @IsNotEmpty()
  @IsString()
  subject: string

  @IsNotEmpty()
  @IsString()
  chatId: string
}