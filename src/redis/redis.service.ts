import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common'
import { RedisClientType, SetOptions, createClient } from 'redis'

@Injectable()
export class RedisService implements OnModuleInit, OnModuleDestroy {
  private client: RedisClientType

  async onModuleInit () {
    this.client = createClient({
      url: process.env.REDIS_SERVER_URL
    })

    this.client.on('error', (err) => console.error('Redis Client Error', err))
    await this.client.connect()
  }

  async onModuleDestroy () {
    await this.client.quit()
  }

  async set (key: string, value: string, mode?: string, duration?: number) {
    if (mode && duration) {
      const options: SetOptions = { [mode]: duration }
      await this.client.set(key, value, options)
    } else {
      await this.client.set(key, value)
    }
  }

  async get (key: string): Promise<string | null> {
    return this.client.get(key)
  }

  async del (key: string): Promise<number> {
    return this.client.del(key)
  }

  async getTTL (key: string): Promise<number | null> {
    const ttl = await this.client.ttl(key)
    return ttl >= 0 ? ttl : null
  }

  async isTokenExpired (key: string): Promise<boolean> {
    const ttl = await this.getTTL(key)
    return ttl === null || ttl <= 0
  }

  async isAccessTokenExpired (userId: string): Promise<boolean> {
    const key = `accessToken:${userId}`
    const isExpired = await this.isTokenExpired(key)
    return isExpired
  }

  async isRefreshTokenExpired (userId: string): Promise<boolean> {
    const key = `refreshToken:${userId}`
    const isExpired = await this.isTokenExpired(key)
    return isExpired
  }
}