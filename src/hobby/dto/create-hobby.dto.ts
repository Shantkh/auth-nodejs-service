import { Type } from 'class-transformer'
import { IsArray, IsNotEmpty, IsString, ValidateNested } from 'class-validator'

export class HobbyInfoDTO {
  @IsString()
  @IsNotEmpty()
  content: string
}

export class CreateHobbyDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => HobbyInfoDTO)
  hobbies: HobbyInfoDTO[]

  @IsString()
  @IsNotEmpty()
  userId: string
}