import { Controller, Body, Patch, Param, Delete, UsePipes, ValidationPipe, HttpException, HttpStatus } from '@nestjs/common'
import { PhoneService } from './phone.service'
import { UpdatePhoneDTO } from './dto/update-phone.dto'

@Controller('phone')
export class PhoneController {
  constructor(private readonly phoneService: PhoneService) {}

  @UsePipes(new ValidationPipe())
  @Patch(':id')
  async updatePhone (@Param('id') id: string, @Body() dto: UpdatePhoneDTO) {
    try {
      return await this.phoneService.updatePhone(id, dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Delete(':id')
  async deletePhone (@Param('id') id: string) {
    return this.phoneService.deletePhone(id)
  }
}
