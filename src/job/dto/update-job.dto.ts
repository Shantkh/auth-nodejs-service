import { PartialType } from '@nestjs/mapped-types'
import { IsNotEmpty, IsString } from 'class-validator'
import { CreateJobDTO } from './create-job.dto'

export class UpdateJobDTO extends PartialType(CreateJobDTO) {
  @IsString()
  @IsNotEmpty()
  id: string
}