import { Type } from 'class-transformer'
import { IsArray, IsNotEmpty, IsOptional, IsString, ValidateNested } from 'class-validator'

export class EducationInfoDTO {
  @IsString()
  @IsNotEmpty()
  degree: string

  @IsString()
  @IsNotEmpty()
  educationalInstitution: string

  @IsString()
  @IsNotEmpty()
  field: string

  @IsString()
  @IsNotEmpty()
  graduationYear: string

  @IsString()
  @IsOptional()
  additionalThings?: string
}

export class CreateEducationDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => EducationInfoDTO)
  educations: EducationInfoDTO[]

  @IsString()
  @IsNotEmpty()
  userId: string
}