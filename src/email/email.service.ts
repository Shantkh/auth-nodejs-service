import { Injectable } from '@nestjs/common'
import { CreateEmailDTO } from './dto/create-email.dto'
import { UpdateEmailDTO } from './dto/update-email.dto'
import { PrismaService } from 'src/prisma.service'

@Injectable()
export class EmailService {
  constructor (private prisma: PrismaService) {}

  // async createEmail (dto: CreateEmailDTO) {
  //   return this.prisma.email.create({
  //     data: dto
  //   })
  // }

  async updateEmail (id: string, dto: UpdateEmailDTO) {
    return this.prisma.email.update({
      where: { id },
      data: dto
    })
  }

  async deleteEmail (id: string) {
    return this.prisma.email.delete({ where: { id } })
  }
}