import { PartialType } from '@nestjs/mapped-types'
import { IsNotEmpty, IsString } from 'class-validator'
import { CreateSkillDTO } from './create-skill.dto'

export class UpdateSkillDTO extends PartialType(CreateSkillDTO) {
  @IsString()
  @IsNotEmpty()
  id: string
}