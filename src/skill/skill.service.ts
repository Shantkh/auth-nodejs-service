import { Injectable } from '@nestjs/common'
import { CreateSkillDTO } from './dto/create-skill.dto'
import { UpdateSkillDTO } from './dto/update-skill.dto'
import { PrismaService } from 'src/prisma.service'

@Injectable()
export class SkillService {
  constructor (private prisma: PrismaService) {}

  async createSkill (dto: CreateSkillDTO) {
    const existingSkills = await this.prisma.skill.findMany({
      where: {
        userId: dto.userId,
        name: { in: dto.skills.map(skill => skill.name) } 
      }
    })

    const existingSkillsMap = new Map(
      existingSkills.map(skill => [skill.name, skill])
    )

    const newSkills = await Promise.all(
      dto.skills.map(async (skillData) => {
        if (!existingSkillsMap.has(skillData.name)) {
          return this.prisma.skill.create({
            data: {
              user: { connect: { id: dto.userId } },
              name: skillData.name,
              level: skillData.level,
              experience: skillData.experience,
              certification: skillData.certification
            }
          })
        }
        return null
      })
    )

    return newSkills.filter(skill => skill !== null)
  }

  async updateSkill (id: string, dto: UpdateSkillDTO) {
    const updatedSkill = await this.prisma.skill.update({
      where: { id }, data: dto
    })
    return updatedSkill
  }

  async deleteSkill (id: string) {
    const deletedSkill = await this.prisma.skill.delete({
      where: { id }
    })
    return deletedSkill.id
  }
}