import { Injectable } from '@nestjs/common'
import { CreateLanguageDTO } from './dto/create-language.dto'
import { UpdateLanguageDTO } from './dto/update-language.dto'
import { PrismaService } from 'src/prisma.service'

@Injectable()
export class LanguageService {
  constructor (private prisma: PrismaService) {}

  async createLanguage (dto: CreateLanguageDTO) {
    const language = await Promise.all(
      dto.languages.map(async languageData => {
        const existingLanguage = await this.prisma.language.findFirst({
          where: {
            userId: dto.userId,
            name: languageData.name
          }
        })

        if (!existingLanguage) {
          return await this.prisma.language.create({
            data: {
              ...languageData,
              userId: dto.userId
            }
          })
        }
      })
    )
    return language
  }

  async updateLanguage (id: string, dto: UpdateLanguageDTO) {
    const updatedLanguage = await this.prisma.language.update({
      where: { id }, data: dto
    })
    return updatedLanguage
  }

  async deleteLanguage (id: string) {
    const deletedLanguage = await this.prisma.language.delete({
      where: { id }
    })
    return deletedLanguage.id
  }
}