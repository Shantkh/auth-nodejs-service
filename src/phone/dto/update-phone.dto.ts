import { IsString, IsNotEmpty } from 'class-validator'

export class UpdatePhoneDTO {
  @IsString()
  @IsNotEmpty()
  number?: string
}