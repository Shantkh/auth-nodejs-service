import { Controller, Get, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe, HttpException, HttpStatus } from '@nestjs/common'
import { HobbyService } from './hobby.service'
import { CreateHobbyDTO } from './dto/create-hobby.dto'
import { UpdateHobbyDTO } from './dto/update-hobby.dto'

@Controller('hobby')
export class HobbyController {
  constructor(private readonly hobbyService: HobbyService) {}

  @UsePipes(new ValidationPipe())
  @Post()
  async createHobby (@Body() dto: CreateHobbyDTO) {
    try {
      return await this.hobbyService.createHobby(dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Patch(':id')
  async updateHobby (@Param('id') id: string, @Body() dto: UpdateHobbyDTO) {
    try {
      return await this.hobbyService.updateHobby(id, dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Delete(':id')
  async deleteHobby (@Param('id') id: string) {
    return this.hobbyService.deleteHobby(id)
  }
}
