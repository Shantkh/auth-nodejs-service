import { Injectable } from '@nestjs/common'
import { CreateHobbyDTO } from './dto/create-hobby.dto'
import { UpdateHobbyDTO } from './dto/update-hobby.dto'
import { PrismaService } from 'src/prisma.service'

@Injectable()
export class HobbyService {
  constructor (private prisma: PrismaService) {}

  async createHobby (dto: CreateHobbyDTO) {
    const hobby = await Promise.all(
      dto.hobbies.map(async hobbyData => {
        const existingHobby = await this.prisma.hobby.findFirst({
          where: {
            userId: dto.userId,
            content: hobbyData.content
          }
        })

        if (!existingHobby) {
          return await this.prisma.hobby.create({
            data: {
              content: hobbyData.content,
              userId: dto.userId
            }
          })
        }
      })
    )
    return hobby
  }

  async updateHobby (id: string, dto: UpdateHobbyDTO) {
    const updatedHobby = await this.prisma.hobby.update({
      where: { id }, data: dto
    })
    return updatedHobby
  }

  async deleteHobby (id: string) {
    const deletedHobby = await this.prisma.hobby.delete({
      where: { id }
    })
    return deletedHobby.id
  }
}