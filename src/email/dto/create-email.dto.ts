import { IsString, IsNotEmpty, IsEmail } from 'class-validator'

export class CreateEmailDTO {
  @IsString()
  @IsNotEmpty()
  userId: string

  @IsString()
  @IsEmail()
  @IsNotEmpty()
  address: string
}