import { PartialType } from '@nestjs/mapped-types'
import { IsNotEmpty, IsString } from 'class-validator'
import { CreateEducationDTO } from './create-education.dto'

export class UpdateEducationDTO extends PartialType(CreateEducationDTO) {
  @IsString()
  @IsNotEmpty()
  id: string
}