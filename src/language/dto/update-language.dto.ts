import { IsOptional, IsString } from 'class-validator'

export class UpdateLanguageDTO {
  @IsString()
  @IsOptional()
  name?: string

  @IsString()
  @IsOptional()
  level?: string
}