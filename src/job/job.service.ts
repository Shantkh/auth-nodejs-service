import { Injectable } from '@nestjs/common'
import { CreateProfessionalInfoDTO } from './dto/create-job.dto'
import { UpdateJobDTO } from './dto/update-job.dto'
import { PrismaService } from 'src/prisma.service'
import { UserService } from 'src/user/user.service'

@Injectable()
export class JobService {
  constructor (private prisma: PrismaService, private userService: UserService) {}

  async createJob (dto: CreateProfessionalInfoDTO) {
    await Promise.all(
      dto.jobs.map(async jobData => {
        const existingJob = await this.prisma.job.findFirst({
          where: {
            userId: dto.userId,
            title: jobData.title,
            company: jobData.company,
            location: jobData.location,
            start: jobData.start,
            end: jobData.end
          }
        })

        if (!existingJob) {
          return await this.prisma.job.create({
            data: {
              title: jobData.title,
              company: jobData.company,
              location: jobData.location,
              start: jobData.start,
              end: jobData.end,
              description: jobData.description,
              user: { connect: { id: dto.userId } }
            }
          })
        }
      })
    )

    if (dto.currentJob !== '') {
      await this.userService.updateUser(dto.userId, { currentJob: dto.currentJob })
    }

    if (dto.currentEmployer !== '') {
      await this.userService.updateUser(dto.userId, { currentEmployer: dto.currentEmployer })
    }

    const user = await this.userService.getById(dto.userId)
    const { password, ...userWithoutPassword } = user
    return userWithoutPassword
  }

  async updateJob (id: string, dto: UpdateJobDTO) {
    const updatedJob = await this.prisma.job.update({
      where: { id }, data: dto
    })
    return updatedJob
  }

  async deleteJob (id: string) {
    const deletedJob = await this.prisma.job.delete({
      where: { id }
    })
    return deletedJob.id
  }
}