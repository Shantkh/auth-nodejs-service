import { Controller, Get, Post, Body, UsePipes, ValidationPipe, HttpStatus, HttpException } from '@nestjs/common'
import { MessageService } from './message.service'
import { MessageDTO } from './dto/message.dto'

@Controller('message')
export class MessageController {
  constructor(private readonly messageService: MessageService) {}

  @UsePipes(new ValidationPipe())
  @Post()
  async createMessage (@Body() dto: MessageDTO) {
    try {
      return await this.messageService.createMessage(dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }
}