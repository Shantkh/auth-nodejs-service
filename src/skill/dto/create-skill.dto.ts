import { Type } from 'class-transformer'
import { IsArray, IsNotEmpty, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator'

export class SkillInfoDTO {
  @IsString()
  @IsNotEmpty()
  name: string

  @IsString()
  @IsNotEmpty()
  level: string

  @IsNumber()
  @IsNotEmpty()
  experience: number

  @IsString()
  @IsOptional()
  certification?: string | null
}

export class CreateSkillDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => SkillInfoDTO)
  skills: SkillInfoDTO[]

  @IsString()
  @IsNotEmpty()
  userId: string
}