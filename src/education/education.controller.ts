import { Controller, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe, HttpException, HttpStatus } from '@nestjs/common'
import { EducationService } from './education.service'
import { CreateEducationDTO } from './dto/create-education.dto'
import { UpdateEducationDTO } from './dto/update-education.dto'

@Controller('education')
export class EducationController {
  constructor(private readonly educationService: EducationService) {}

  @UsePipes(new ValidationPipe())
  @Post()
  async createEducation (@Body() dto: CreateEducationDTO) {
    try {
      return await this.educationService.createEducation(dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Patch(':id')
  async updateEducation (@Param('id') id: string, @Body() dto: UpdateEducationDTO) {
    try {
      return await this.educationService.updateEducation(id, dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Delete(':id')
  async deleteEducation (@Param('id') id: string) {
    return this.educationService.deleteEducation(id)
  }
}
