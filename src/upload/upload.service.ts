import { Injectable } from '@nestjs/common'
import { S3 } from 'aws-sdk'
import * as fs from 'fs'
import { PrismaService } from 'src/prisma.service'

@Injectable()
export class UploadService {
  private readonly s3: S3

  constructor (private prisma: PrismaService) {
    this.s3 = new S3({
      endpoint: process.env.DIGITALOCEAN_ENDPOINT, 
      accessKeyId: process.env.DIGITALOCEAN_ACCESS_KEY, 
      secretAccessKey: process.env.DIGITALOCEAN_SECRET_KEY, 
      region: process.env.DIGITALOCEAN_REGION,
      s3ForcePathStyle: true
    })
  }

  async uploadFile (files: { img?: Express.Multer.File[], video?: Express.Multer.File[] }) {
    const uploadFileToSpace = async (file: Express.Multer.File[]) => {
      const originalName = file[0].originalname
      const existingFile = await this.prisma.upload.findUnique({ where: { name: originalName } })
      
      if (existingFile) {
        return existingFile.url
      }
      
      const fileContent = fs.readFileSync(file[0].path)
      const uploadParams = {
        Bucket: process.env.DIGITALOCEAN_BUCKET,
        Key: `${Date.now()}___${originalName}`, 
        Body: fileContent,
        ACL: 'public-read'
      }

      try {
        const data = await this.s3.upload(uploadParams).promise()
        await this.prisma.upload.create({
          data: { url: data.Location, name: originalName }
        })
        fs.unlinkSync(file[0].path)
        return data.Location 
      } catch (err) {
        console.error('Error uploading file:', err)
        throw err
      }
    }

    const img = files.img ? await uploadFileToSpace(files.img) : null
    const video = files.video ? await uploadFileToSpace(files.video) : null

    return img ? img : video
  }  

  async deleteFile (url: string) {
    // console.log('url',url)
    const fileKey = decodeURIComponent(url.split('/').pop())
    // console.log('fileKey',fileKey)
    const originalName = fileKey.split('___')[1]
    // console.log('originalName',originalName)
    await this.prisma.upload.delete({ where: { name: originalName } })

    const deleteParams = {
      Bucket: process.env.DIGITALOCEAN_BUCKET,
      Key: fileKey
    }

    try {
      await this.s3.deleteObject(deleteParams).promise()
    } catch (err) {
      console.error('Error deleting file:', err)
      throw err
    }
  }
}