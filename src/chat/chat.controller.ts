import { Controller, Get, Post, Body, Patch, Param, HttpException, HttpStatus, UsePipes, ValidationPipe } from '@nestjs/common'
import { ChatService } from './chat.service'
import { ChatDTO } from './dto/chat.dto'

@Controller('chat')
export class ChatController {
  constructor(private readonly chatService: ChatService) {}

  @UsePipes(new ValidationPipe())
  @Post()
  async createChat (@Body() dto: ChatDTO) {
    try {
      return await this.chatService.createChat(dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Get('unique/:id')
  async findOne (@Param('id') id: string) {
    try {
      return await this.chatService.findOne(id)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Get(':id')
  async findChats (@Param('id') id: string) {
    try {
      return await this.chatService.findChats(id)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Patch('toggle/important') 
  toggleImportant (@Body() { userId, chatId }: { userId: string; chatId: string }) {
    try {
      return this.chatService.toggleImportant(userId, chatId)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Patch('toggle/archive') 
  toggleArchive (@Body() { userId, chatId }: { userId: string; chatId: string }) {
    try {
      return this.chatService.toggleArchive(userId, chatId)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Patch('open/:id') 
  openChat (@Param('id') id: string) {
    try {
      return this.chatService.openChat(id)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }
}