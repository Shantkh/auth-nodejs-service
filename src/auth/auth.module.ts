import { Module } from '@nestjs/common'
import { AuthService } from './auth.service'
import { AuthController } from './auth.controller'
import { GoogleStrategy } from './strategies/google.strategy'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { JwtModule } from '@nestjs/jwt'
import { PassportModule } from '@nestjs/passport'
import { JwtStrategy } from './strategies/jwt.strategy'
import { UserService } from 'src/user/user.service'
import { PrismaService } from 'src/prisma.service'
import { SendGridModule } from '@anchan828/nest-sendgrid'
import { RedisService } from 'src/redis/redis.service'
import { getJwtConfig } from 'configs/jwt.config'
import { UploadService } from 'src/upload/upload.service'

@Module({
  imports: [
    ConfigModule.forRoot(), 
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: getJwtConfig
    }),
    SendGridModule.forRoot({
      apikey: process.env.SENDGRID_API_KEY
    })
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, GoogleStrategy, UserService, PrismaService, RedisService, UploadService]
})
export class AuthModule {}