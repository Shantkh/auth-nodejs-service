import { IsOptional, IsString } from 'class-validator'

export class UpdateContactDTO {
  @IsString()
  @IsOptional()
  street?: string

  @IsString()
  @IsOptional()
  province?: string

  @IsString()
  @IsOptional()
  country?: string

  @IsString()
  @IsOptional()
  city?: string

  @IsString()
  @IsOptional()
  postalCode?: string
}