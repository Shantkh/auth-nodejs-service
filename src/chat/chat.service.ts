import { Injectable } from '@nestjs/common'
import { ChatDTO } from './dto/chat.dto'
import { PrismaService } from 'src/prisma.service'
import { MessageService } from 'src/message/message.service'

@Injectable()
export class ChatService {
  constructor (private prisma: PrismaService, private messageService: MessageService) {}

  async createChat (dto: ChatDTO) {
    const { users, message } = dto

    const existingChat = await this.prisma.chat.findFirst({
      where: {
        AND: [
          { users: { some: { id: users[0] } } },
          { users: { some: { id: users[1] } } }
        ]
      },
      include: { users: { select: { id: true, firstName: true, lastName: true, email: true } } }
    })

    if (existingChat) {
      const newMessage = await this.messageService.createMessage({ ...message, chatId: existingChat.id })
      
      if (newMessage) {
        const updatedChat = await this.prisma.chat.update({
          where: { id: existingChat.id },
          data: { updatedAt: new Date() },
          include: { users: { select: { id: true, firstName: true, lastName: true, email: true } }, messages: true }
        })
    
        return updatedChat
      }
    } else {
      const newChat = await this.prisma.chat.create({
        data: { users: { connect: users.map(id => ({ id })) } },
        include: { users: { select: { id: true, firstName: true, lastName: true, email: true } }, messages: true }
      })
      const newMessage = await this.messageService.createMessage({ ...message, chatId: newChat.id })

      if (newMessage) {
        const updatedChat = await this.prisma.chat.update({
          where: { id: newChat.id },
          data: {
            messages: { connect: { id: newMessage.id } },
            updatedAt: new Date()
          },
          include: { users: { select: { id: true, firstName: true, lastName: true, email: true } }, messages: true }
        })
    
        return updatedChat
      }
    }
  }

  async findOne (id: string) {
    return this.prisma.chat.findUnique({
      where: { id }, 
      include: { users: { select: { id: true, firstName: true, lastName: true, email: true } }, messages: true }
    })
  }

  async findChats(id: string) {
    const chats = await this.prisma.chat.findMany({
      where: { users: { some: { id } } },
      include: { 
        users: { select: { id: true, firstName: true, lastName: true, email: true } },
        messages: true,
        importantBy: { where: { userId: id }, select: { id: true, chatId: true } },
        archivedBy: { where: { userId: id }, select: { id: true, chatId: true } }
      },
      orderBy: { updatedAt: 'desc' }
    })
  
    return chats.map(chat => ({
      ...chat,
      important: chat.importantBy.length > 0,
      archived: chat.archivedBy.length > 0
    }))
  }

  async toggleImportant (userId: string, chatId: string) {
    const existingRecord = await this.prisma.importantChat.findUnique({
      where: { 
        userId_chatId: { userId, chatId }
      }
    })
  
    if (existingRecord) {
      await this.prisma.importantChat.delete({
        where: { 
          userId_chatId: { userId, chatId }
        }
      })
    } else {
      await this.prisma.importantChat.create({
        data: { userId, chatId }
      })
    }
  
    return this.prisma.chat.findUnique({
      where: { id: chatId },
      include: { importantBy: true }
    })
  }

  async toggleArchive (userId: string, chatId: string) {
    const existingRecord = await this.prisma.archivedChat.findUnique({
      where: { 
        userId_chatId: { userId, chatId }
      }
    })

    if (existingRecord) {
      await this.prisma.archivedChat.delete({
        where: { 
          userId_chatId: { userId, chatId }
        }
      })
    } else {
      await this.prisma.archivedChat.create({
        data: { userId, chatId }
      })
    }
  
    return this.prisma.chat.findUnique({
      where: { id: chatId },
      include: { importantBy: true }
    })
  }

  async openChat (id: string) {
    const chat = await this.prisma.chat.findUnique({
      where: { id },
      select: { opened: true }
    })

    if (!chat.opened) {
      return this.prisma.chat.update({
        where: { id },
        data: { opened: true }
      })
    }
  }
}