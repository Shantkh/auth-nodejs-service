import { Controller, Post, UseInterceptors, UploadedFiles, HttpStatus, HttpException } from '@nestjs/common'
import { UploadService } from './upload.service'
import { FileFieldsInterceptor } from '@nestjs/platform-express'

@Controller('upload')
export class UploadController {
  constructor(private readonly uploadService: UploadService) {}

  @Post()
  @UseInterceptors(FileFieldsInterceptor([ { name: 'img', maxCount: 1 }, { name: 'video', maxCount: 1 } ]))
  async uploadFile (@UploadedFiles() files: { img?: Express.Multer.File[], video?: Express.Multer.File[] }) {
    try {
      return await this.uploadService.uploadFile(files)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }
}