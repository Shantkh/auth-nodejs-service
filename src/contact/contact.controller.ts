import { Controller, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe, HttpStatus, HttpException } from '@nestjs/common'
import { ContactService } from './contact.service'
import { CreatePersonalInfoDTO } from './dto/create-contact.dto'
import { UpdateContactDTO } from './dto/update-contact.dto'

@Controller('contact')
export class ContactController {
  constructor(private readonly contactService: ContactService) {}

  @UsePipes(new ValidationPipe())
  @Post()
  async createContact (@Body() dto: CreatePersonalInfoDTO) {
    try {
      return await this.contactService.createContact(dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Patch(':id')
  async updateContact (@Param('id') id: string, @Body() dto: UpdateContactDTO) {
    try {
      return await this.contactService.updateContact(id, dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Delete(':id')
  async deleteContact (@Param('id') id: string) {
    return this.contactService.deleteContact(id)
  }
}