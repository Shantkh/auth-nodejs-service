import { Injectable, UnauthorizedException, NotFoundException, BadRequestException } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { verify } from 'argon2'
import { UserService } from 'src/user/user.service'
import { UserCreateDTO } from 'src/user/dto/create-user.dto'
import { AuthDTO } from './dto/auth-dto'
import { SendGridService } from '@anchan828/nest-sendgrid'
import { RedisService } from 'src/redis/redis.service'
import { UserChangePasswordDTO } from 'src/user/dto/change-password-user.dto'
import { Response } from 'express'
import { AccessTokenDTO } from './dto/access-token-dto'

@Injectable()
export class AuthService {
  constructor (
    private jwt: JwtService,
    private userService: UserService,
    // private readonly sendGrid: SendGridService,
    private readonly redisService: RedisService,
  ) {}

  async logout (userId: string) {
    await this.redisService.del(`accessToken:${userId}`)
    await this.redisService.del(`refreshToken:${userId}`)
    return 'success'
  }

  async login (dto: AuthDTO, res: Response) {
    const user = await this.validateUser(dto)
    const tokens = this.issueTokens(user.id, user.email)
    await this.storeTokensInRedis(user.id, tokens)
    res.cookie('userId', user.id, { httpOnly: true, secure: true })
    return user
  }

  async register (dto: UserCreateDTO) {
    const oldUser = await this.userService.getByEmail(dto.email)
    if (oldUser) throw new BadRequestException('user already exists!')
    const { password, ...user } = await this.userService.createUser(dto)
    let digits = '0123456789'
    let OTP = ''
    for (let i = 0; i < 6; i++) {
      OTP += digits[Math.floor(Math.random() * 10)]
    }
    await this.sendVerificationEmail(user, OTP)
    return { OTP, id: user.id }
  }

  private issueTokens (id: string, email: string) {
    const data = { id, email }
    const role = process.env.ADMIN_ROLE
    const dataForRefresh = { id, email, role }
    const accessToken = this.jwt.sign(data)
    const refreshToken = this.jwt.sign(dataForRefresh)
    return { accessToken, refreshToken }
  }

  private async validateUser (dto: AuthDTO) {
    const user = await this.userService.getByEmail(dto.email)
    // if (!user) throw new NotFoundException('user not found')
    const isValid = await verify(user.password, dto.password)
    if (!isValid) throw new UnauthorizedException('Invalid email or password. Please try again.')
    // if (!user.verified) throw new UnauthorizedException('Please verify email.')
    // return user
    const { password, ...userWithoutPassword } = user
    return userWithoutPassword
  }

  private async sendVerificationEmail (user: object, OTP: string) {
    console.log('00000000000000000000000:',user)
    console.log('11111111111111111111111:',OTP)
    // await this.sendGrid.send({
    //   to: "gurekhyanmovses2001@gmail.com",
    //   from: "info@balians-it.com",
    //   subject: "Sending with SendGrid is Fun",
    //   html: "and easy to do anywhere, even with Node.js"
    // })
    // .then(() => console.log('Message sent successfully!'))
    // .catch(err => console.log('Error:', err))
  }

  private async storeTokensInRedis(userId: string, tokens: { accessToken: string, refreshToken: string }) {
    const { accessToken, refreshToken } = tokens
    await this.redisService.set(`accessToken:${userId}`, accessToken, 'EX', 604800) 
    await this.redisService.set(`refreshToken:${userId}`, refreshToken, 'EX', 18144000) 
  }

  async changePassword (dto: UserChangePasswordDTO) {
    const user = await this.userService.getByEmail(dto.email)
    if (!user) throw new NotFoundException('User not found')
    await this.userService.updateUser(user.id, { password: dto.newPassword })
    return { message: 'Password successfully changed!'}
  }

  async generateNewAccessToken (dto: AccessTokenDTO) {
    const accessToken = this.jwt.sign(dto)
    await this.redisService.set(`accessToken:${dto.id}`, accessToken, 'EX', 604800) 
  }
}