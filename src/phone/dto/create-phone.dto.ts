import { IsString, IsNotEmpty } from 'class-validator'

export class CreatePhoneDTO {
  @IsString()
  @IsNotEmpty()
  userId: string

  @IsString()
  @IsNotEmpty()
  number: string
}