import { Injectable } from '@nestjs/common'
import { argon2id, hash } from 'argon2'
import { UserCreateDTO } from './dto/create-user.dto'
import { PrismaService } from 'src/prisma.service'
import { UploadService } from 'src/upload/upload.service'

@Injectable()
export class UserService {
  constructor (private prisma: PrismaService, private uploadService: UploadService) {}

  async findAll () {
    return await this.prisma.user.findMany()
  }

  getById (id: string) {
    return this.prisma.user.findUnique(
      {
        where: { id },
        include: {
          chats: true,              
          importantChats: true,     
          archivedChats: true,      
          contacts: true,        
          jobs: true,              
          educations: true,         
          skills: true,             
          languages: true,          
          emails: true,          
          phones: true,
          hobbies: true,          
        }
      }
    )
  }

  getByEmail (email: string) {
    return this.prisma.user.findUnique(
      {
        where: { email },
        include: {
          chats: true,              
          importantChats: true,     
          archivedChats: true,      
          contacts: true,        
          jobs: true,              
          educations: true,         
          skills: true,             
          languages: true,          
          emails: true,          
          phones: true,    
          hobbies: true,    
        }
      }
    )
  }

  async getProfile (id: string) {
    const profile = await this.getById(id) 
    const { password, ...rest } = profile

    return rest
  }

  async createUser (dto: UserCreateDTO) {
    const options = {
      type: argon2id,
      memoryCost: 2 ** 16,  
      timeCost: 3,
      parallelism: 4
    }

    const user = {
      firstName: dto.firstName,
      lastName: dto.lastName,
      email: dto.email,
      password: await hash(dto.password, options),
      role: dto.email === process.env.ADMIN_MAIL ? process.env.ADMIN_ROLE : 'user'
    }
    return this.prisma.user.create({ data: user })
  }

  async updateUser (id: string, dto: any) {
    const data = { ...dto }
    const user = await this.prisma.user.findUnique({ where: { id } })

    if (user.avatar && dto.avatar && user.avatar !== dto.avatar) {
      await this.uploadService.deleteFile(user.avatar)
    }

    return this.prisma.user.update(
      { where: { id }, data }
    )
  }

  async verifyEmail (id: string) {
    await this.prisma.user.update({
      where: { id }, data: { verified: true }
    })
  }
}