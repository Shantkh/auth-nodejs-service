import { Module } from '@nestjs/common'
import { ContactService } from './contact.service'
import { ContactController } from './contact.controller'
import { PrismaService } from 'src/prisma.service'
import { UserService } from 'src/user/user.service'
import { UploadService } from 'src/upload/upload.service'

@Module({
  controllers: [ContactController],
  providers: [ContactService, PrismaService, UserService, UploadService]
})
export class ContactModule {}