import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import * as logger from 'morgan'
// import { createClient } from 'redis'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
//   const redisClient = createClient({
//     url: 'redis://localhost:6379' // Ensure this URL is correct for your setup
//   });
//   redisClient.on('error', (err) => console.log('Redis Client Error', err));
//   await redisClient.connect();

// //   // Set a key-value pair in Redis
//   await redisClient.set('love', 'Makarian');
// console.log('000000000000000000:', await redisClient.get('love'))
// const secretWord = 'mySecret123'
// const message = 'message to hash'
// const combined = secretWord + message
// const hash = sha512(combined)
// console.log('hash:',hash)
  app.setGlobalPrefix('api')
  app.enableCors({
    origin: process.env.CLIENT_URL,
    methods: [ 'GET', 'POST',  'PATCH', 'PUT', 'DELETE' ],
    credentials: true
  })
  app.use(logger('dev'))
  await app.listen(process.env.SERVER_PORT, () => console.log('\n☘️  Server connected!\n'))
}
bootstrap()