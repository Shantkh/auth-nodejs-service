import { IsNotEmpty, IsString } from "class-validator"

export class UpdateHobbyDTO {
  @IsString()
  @IsNotEmpty()
  content: string
}