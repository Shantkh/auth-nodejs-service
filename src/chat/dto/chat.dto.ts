import { Type } from "class-transformer"
import { IsArray, IsOptional, IsString, ValidateNested } from "class-validator"
// import { MessageDTO } from "src/message/dto/message.dto"

class MessageDTO {
  @IsString()
  content: string

  @IsString()
  receiverId: string

  @IsString()
  subject: string
}

export class ChatDTO {
  @IsArray()
  @IsString({ each: true })
  users: string[]

  @IsOptional()
  @ValidateNested()
  @Type(() => MessageDTO)
  message: MessageDTO
}