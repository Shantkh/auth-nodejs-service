import { ArrayMinSize, IsArray, IsNotEmpty, IsString, ValidateNested } from 'class-validator'
import { Type } from 'class-transformer'
import { CreateEmailDTO } from 'src/email/dto/create-email.dto'
import { CreatePhoneDTO } from 'src/phone/dto/create-phone.dto'

export class ContactInfoDTO {
  @IsString()
  @IsNotEmpty()
  street: string

  @IsString()
  @IsNotEmpty()
  province: string

  @IsString()
  @IsNotEmpty()
  country: string

  @IsString()
  @IsNotEmpty()
  city: string

  @IsString()
  @IsNotEmpty()
  postalCode: string
}

export class CreatePersonalInfoDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => ContactInfoDTO)
  contacts: ContactInfoDTO[]

  @IsArray()
  @ArrayMinSize(1)
  @Type(() => CreatePhoneDTO)
  phones: CreatePhoneDTO[]

  @IsArray()
  @ArrayMinSize(1)
  @Type(() => CreateEmailDTO)
  emails: CreateEmailDTO[]

  @IsString()
  @IsNotEmpty()
  userId: string
}