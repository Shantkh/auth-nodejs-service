import { IsString, IsOptional, IsEmail } from 'class-validator'

export class UpdateEmailDTO {
  @IsString()
  @IsOptional()
  userId?: string

  @IsString()
  @IsEmail()
  @IsOptional()
  address?: string
}