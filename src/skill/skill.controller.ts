import { Controller, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe, HttpException, HttpStatus } from '@nestjs/common'
import { SkillService } from './skill.service'
import { CreateSkillDTO } from './dto/create-skill.dto'
import { UpdateSkillDTO } from './dto/update-skill.dto'

@Controller('skill')
export class SkillController {
  constructor(private readonly skillService: SkillService) {}

  @UsePipes(new ValidationPipe())
  @Post()
  async createSkill (@Body() dto: CreateSkillDTO) {
    try {
      return await this.skillService.createSkill(dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Patch(':id')
  async updateSkill (@Param('id') id: string, @Body() dto: UpdateSkillDTO) {
    try {
      return await this.skillService.updateSkill(id, dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Delete(':id')
  async deleteSkill (@Param('id') id: string) {
    return this.skillService.deleteSkill(id)
  }
}