import { Injectable, NestMiddleware } from '@nestjs/common'
import { Request, Response, NextFunction } from 'express'
import * as jwt from 'jsonwebtoken'
import { RedisService } from 'src/redis/redis.service'

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly redisService: RedisService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const { userId } = req.cookies
    
    try {
      const isAccessTokenExpired = await this.redisService.isTokenExpired(`accessToken:${userId}`)
      const isRefreshTokenExpired = await this.redisService.isTokenExpired(`refreshToken:${userId}`)

      if (!isAccessTokenExpired) {
        return next()
      }

      if (!isRefreshTokenExpired) {
        const refreshToken = await this.redisService.get(`refreshToken:${userId}`)
        console.log('refreshToken',refreshToken)
        jwt.verify(refreshToken, process.env.SECRET_KEY, (err: any) => {
          if (err) {
            console.error('JWT verification error:', err)
            return res.status(403).send('Access denied')
          } else {
            return res.status(401).send('AccessTokenRequired')
          }
        })
      } else {
        res.clearCookie('userId')
        return res.status(401).send('LogoutRequired')
      }
    } catch (error) {
      console.error('Error:', error)
      return res.status(500).send('Internal Server Error')
    }
  }
}