import { Controller, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe, HttpStatus, HttpException } from '@nestjs/common'
import { LanguageService } from './language.service'
import { CreateLanguageDTO } from './dto/create-language.dto'
import { UpdateLanguageDTO } from './dto/update-language.dto'

@Controller('language')
export class LanguageController {
  constructor(private readonly languageService: LanguageService) {}

  @UsePipes(new ValidationPipe())
  @Post()
  async createLanguage (@Body() dto: CreateLanguageDTO) {
    try {
      return await this.languageService.createLanguage(dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Patch(':id')
  async updateLanguage (@Param('id') id: string, @Body() dto: UpdateLanguageDTO) {
    try {
      return await this.languageService.updateLanguage(id, dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Delete(':id')
  async deleteLanguage (@Param('id') id: string) {
    return this.languageService.deleteLanguage(id)
  }
}