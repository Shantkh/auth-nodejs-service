import { Injectable } from '@nestjs/common'
import { CreateEducationDTO } from './dto/create-education.dto'
import { UpdateEducationDTO } from './dto/update-education.dto'
import { PrismaService } from 'src/prisma.service'

@Injectable()
export class EducationService {
  constructor (private prisma: PrismaService) {}

  async createEducation (dto: CreateEducationDTO) {
    const existingEducations = await this.prisma.education.findMany({
      where: {
        userId: dto.userId,
        educationalInstitution: { in: dto.educations.map(education => education.educationalInstitution) }
      }
    })

    const existingEducationsMap = new Map(
      existingEducations.map(education => [education.educationalInstitution, education])
    )

    const newEducations = await Promise.all(
      dto.educations.map(async (educationData) => {
        if (!existingEducationsMap.has(educationData.educationalInstitution)) {
          return this.prisma.education.create({
            data: {
              user: { connect: { id: dto.userId } },
              educationalInstitution: educationData.educationalInstitution,
              degree: educationData.degree,
              field: educationData.field,
              graduationYear: educationData.graduationYear,
              additionalThings: educationData.additionalThings
            }
          })
        }
        return null
      })
    )

    return newEducations.filter(education => education !== null)
  }

  async updateEducation (id: string, dto: UpdateEducationDTO) {
    const updatedEducation = await this.prisma.education.update({
      where: { id }, data: dto
    })
    return updatedEducation
  }

  async deleteEducation (id: string) {
    const deletedEducation = await this.prisma.education.delete({
      where: { id }
    })
    return deletedEducation.id
  }
}