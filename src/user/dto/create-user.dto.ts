import { IsEmail, IsString, MaxLength, MinLength } from "class-validator"

export class UserCreateDTO {
  @IsString()
  firstName: string

  @IsString()
  lastName: string

  @IsString()
  @IsEmail()
  email: string
  
  @MinLength(8, { message: 'Password must be at least 8 characters long' })
  @MaxLength(20, { message: 'Password must not exceed 20 characters' })
  @IsString()
  password: string
}