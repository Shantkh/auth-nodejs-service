import { Injectable } from '@nestjs/common'
import { CreatePersonalInfoDTO } from './dto/create-contact.dto'
import { UpdateContactDTO } from './dto/update-contact.dto'
import { PrismaService } from 'src/prisma.service'
import { UserService } from 'src/user/user.service'

@Injectable()
export class ContactService {
  constructor (private prisma: PrismaService, private userService: UserService) {}

  async createContact (dto: CreatePersonalInfoDTO) {
    await Promise.all(
      dto.contacts.map(async contactData => {
        const existingContact = await this.prisma.contact.findFirst({
          where: {
            userId: dto.userId,
            street: contactData.street,
            province: contactData.province,
            country: contactData.country,
            city: contactData.city,
            postalCode: contactData.postalCode
          }
        })

        if (!existingContact) {
          return await this.prisma.contact.create({
            data: {
              ...contactData,
              user: { connect: { id: dto.userId } }
            }
          })
        }
      })
    )

    const validPhones = dto.phones.filter(phone => phone.number?.trim() !== '')
    await Promise.all(
      validPhones.map(async phone => {
        const existingPhone = await this.prisma.phone.findFirst({
          where: {
            userId: dto.userId,
            number: phone.number
          }
        })
  
        if (!existingPhone) {
          return await this.prisma.phone.create({
            data: {
              number: phone.number,
              user: { connect: { id: dto.userId } }
            }
          })
        }
      })
    )
  
    const validEmails = dto.emails.filter(email => email.address?.trim() !== '')
    await Promise.all(
      validEmails.map(async email => {
        const existingEmail = await this.prisma.email.findFirst({
          where: {
            userId: dto.userId,
            address: email.address
          }
        })
  
        if (!existingEmail) {
          return await this.prisma.email.create({
            data: {
              address: email.address,
              user: { connect: { id: dto.userId } }
            }
          })
        }
      })
    )

    const user = await this.userService.getById(dto.userId)
    const { password, ...userWithoutPassword } = user
    return userWithoutPassword
  }

  async updateContact (id: string, dto: UpdateContactDTO) {
    const updatedContact = await this.prisma.contact.update({
      where: { id }, data: dto
    })
    return updatedContact
  }

  async deleteContact (id: string) {
    const deletedContact = await this.prisma.contact.delete({
      where: { id }
    })
    return deletedContact.id
  }
}