import { Body, Controller, Get, HttpException, HttpStatus, Param, Patch, Put, UsePipes, ValidationPipe } from '@nestjs/common'
import { UserService } from './user.service'

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('all')
  async findAll () {
    try {
      return await this.userService.findAll()
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Get(':id')
  async getProfile (@Param('id') id: string) {
    try {
      return await this.userService.getProfile(id)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Put(':id')
  async updateUser (@Param('id') id: string, @Body() dto: any) {
    try {
      return await this.userService.updateUser(id, dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Patch(':id')
  async verifyEmail (@Param('id') id: string) {
    try {
      return await this.userService.verifyEmail(id)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }
}