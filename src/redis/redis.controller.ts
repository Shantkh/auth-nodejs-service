import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common'
import { RedisService } from './redis.service'

@Controller('redis')
export class RedisController {
  constructor(private readonly redisService: RedisService) {}

  // @Post('set')
  // async setKey (@Body() body: { key: string; value: string; expiration?: number }) {
  //   const { key, value, expiration } = body
  //   if (expiration) {
  //     await this.redisService.set(key, value, 'EX', expiration)
  //   } else {
  //     await this.redisService.set(key, value)
  //   }
  //   return { message: `Key ${key} set successfully` }
  // }

  // @Get('get/:key')
  // async getKey (@Param('key') key: string) {
  //   const value = await this.redisService.get(key)
  //   if (value === null) {
  //     return { message: `Key ${key} not found` }
  //   }
  //   return { key, value }
  // }

  // @Delete('delete/:key')
  // async deleteKey (@Param('key') key: string) {
  //   await this.redisService.del(key)
  //   return { message: `Key ${key} deleted successfully` }
  // }

  // @Get('isExpired/:key')
  // async isTokenExpired (@Param('key') key: string) {
  //   const expired = await this.redisService.isTokenExpired(key)
  //   console.log('expired',expired)
  //   return { key, expired }
  // }
}
