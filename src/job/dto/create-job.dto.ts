import { IsArray, IsNotEmpty, IsOptional, IsString, ValidateNested } from 'class-validator'
import { Type } from 'class-transformer'

export class CreateJobDTO {
  @IsString()
  @IsNotEmpty()
  title: string

  @IsString()
  @IsNotEmpty()
  company: string

  @IsString()
  @IsNotEmpty()
  location: string

  @IsString()
  @IsNotEmpty()
  start: string

  @IsString()
  @IsNotEmpty()
  end: string

  @IsString()
  @IsOptional()
  description?: string
}

export class CreateProfessionalInfoDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateJobDTO)
  jobs: CreateJobDTO[]

  @IsString()
  currentJob: string

  @IsString()
  currentEmployer: string

  @IsString()
  @IsNotEmpty()
  userId: string
}