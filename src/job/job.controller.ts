import { Controller, Post, Body, Patch, Param, Delete, HttpException, HttpStatus, ValidationPipe, UsePipes } from '@nestjs/common'
import { JobService } from './job.service'
import { CreateProfessionalInfoDTO } from './dto/create-job.dto'
import { UpdateJobDTO } from './dto/update-job.dto'

@Controller('job')
export class JobController {
  constructor(private readonly jobService: JobService) {}

  @UsePipes(new ValidationPipe())
  @Post()
  async createJob (@Body() dto: CreateProfessionalInfoDTO) {
    try {
      return await this.jobService.createJob(dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Patch(':id')
  async updateJob (@Param('id') id: string, @Body() dto: UpdateJobDTO) {
    try {
      return await this.jobService.updateJob(id, dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @Delete(':id')
  async deleteJob (@Param('id') id: string) {
    return this.jobService.deleteJob(id)
  }
}