import { Controller, Post, Body, Get, UseGuards, Patch, Res, HttpStatus, UsePipes, ValidationPipe, HttpException, Req } from '@nestjs/common'
import { AuthService } from './auth.service'
import { UserCreateDTO } from 'src/user/dto/create-user.dto'
import { AuthDTO } from './dto/auth-dto'
import { GoogleAuthGuard } from './guards/google.guard'
import { UserChangePasswordDTO } from 'src/user/dto/change-password-user.dto'
import { Request, Response } from 'express'
import { AccessTokenDTO } from './dto/access-token-dto'

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Get('google')
  @UseGuards(GoogleAuthGuard)
  async googleLogin () {
    return 'OK'
  }

  @Get('google/callback')
  @UseGuards(GoogleAuthGuard)
  async googleCallback () {
    return 'OK'
  }

  @UsePipes(new ValidationPipe())
  @Post('logout')
  async logout (@Body() dto: string, @Req() req: Request, @Res({ passthrough: true }) res: Response) {
    try {
      // return await this.authService.login(dto, res)
      res.clearCookie('userId')
      req.logout(err => {
        if (err) {
          return res.status(500).json({ message: err.message })
        }
      })
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Post('login')
  async login (@Body() dto: AuthDTO, @Res({ passthrough: true }) res: Response) {
    try {
      return await this.authService.login(dto, res)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Post('login/refresh')
  async generateNewAccessToken (@Body() dto: AccessTokenDTO) {
    try {
      return await this.authService.generateNewAccessToken(dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Post('register')
  async register (@Body() dto: UserCreateDTO) {
    try {
      return await this.authService.register(dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }

  @UsePipes(new ValidationPipe())
  @Patch('change-password')
  async changePassword (@Body() dto: UserChangePasswordDTO) {
    try {
      return await this.authService.changePassword(dto)
    } catch (err) {
      console.log('Error:', err.message)
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
    }
  }
}