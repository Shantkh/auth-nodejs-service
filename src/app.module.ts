import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common'
import { AuthModule } from './auth/auth.module'
import { UserModule } from './user/user.module'
import { ConfigModule } from '@nestjs/config'
import { RedisModule } from './redis/redis.module'
import { AuthMiddleware } from 'middlewares/auth'
import { ChatModule } from './chat/chat.module'
import { MessageModule } from './message/message.module'
import * as cookieParser from 'cookie-parser'
import { UploadModule } from './upload/upload.module'
import { ContactModule } from './contact/contact.module'
import { EmailModule } from './email/email.module';
import { PhoneModule } from './phone/phone.module';
import { HobbyModule } from './hobby/hobby.module';
import { LanguageModule } from './language/language.module';
import { SkillModule } from './skill/skill.module';
import { EducationModule } from './education/education.module';
import { JobModule } from './job/job.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    AuthModule, 
    UserModule, 
    RedisModule, 
    ChatModule, 
    MessageModule,
    UploadModule,
    ContactModule,
    EmailModule,
    PhoneModule,
    HobbyModule,
    LanguageModule,
    SkillModule,
    EducationModule,
    JobModule
  ]
})

export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
    .apply(cookieParser(), AuthMiddleware)
    .forRoutes(
      { path: 'user/all', method: RequestMethod.ALL },
      { path: 'user/:id', method: RequestMethod.ALL },
    )
  }
}