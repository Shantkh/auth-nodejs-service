import { Module } from '@nestjs/common'
import { HobbyService } from './hobby.service'
import { HobbyController } from './hobby.controller'
import { PrismaService } from 'src/prisma.service'

@Module({
  controllers: [HobbyController],
  providers: [HobbyService, PrismaService],
})
export class HobbyModule {}