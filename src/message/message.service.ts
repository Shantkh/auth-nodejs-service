import { Injectable } from '@nestjs/common'
import { MessageDTO } from './dto/message.dto'
import { PrismaService } from 'src/prisma.service'

@Injectable()
export class MessageService {
  constructor (private prisma: PrismaService) {}
  
  async createMessage (dto: MessageDTO) {
    return await this.prisma.message.create({
      data: {
        content: dto.content,
        receiverId: dto.receiverId,
        subject: dto.subject,
        chat: { connect: { id: dto.chatId } }
      }
    })
  }
}