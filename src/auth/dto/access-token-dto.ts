import { IsEmail, IsString } from 'class-validator'

export class AccessTokenDTO {
  @IsString()
  id: string

  @IsEmail()
  email: string
}