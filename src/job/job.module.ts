import { Module } from '@nestjs/common'
import { JobService } from './job.service'
import { JobController } from './job.controller'
import { PrismaService } from 'src/prisma.service'
import { UserService } from 'src/user/user.service'
import { UploadService } from 'src/upload/upload.service'

@Module({
  controllers: [JobController],
  providers: [JobService, PrismaService, UserService, UploadService]
})
export class JobModule {}